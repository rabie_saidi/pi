package pi.optimisation;

import la.matrix.Matrix;
import pi.core.prediction.PredictionSet;
import scala.Serializable;

/**
 * @author Rabie Saidi
 */
public interface Optimiser  extends Serializable {
    PredictionSet predict();
    OptimisationMatrix getMatrix();
}
