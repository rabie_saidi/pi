package pi.optimisation;

import la.decomposition.SingularValueDecomposition;
import la.matrix.DenseMatrix;
import la.matrix.Matrix;

import static ml.utils.Printer.disp;
import static ml.utils.Printer.fprintf;

/**
 * @author Rabie SAIDI
 */
public class SVDExample_LAML {
    public static void main(String[] args) {
        SingularValueDecomposition svdImpl = new SingularValueDecomposition(new DenseMatrix(ExampleData.data).transpose());
        Matrix U = svdImpl.getU();
        Matrix S = svdImpl.getS();
        Matrix V = svdImpl.getV();
        fprintf("U:\n");
        disp(U, 4);
        fprintf("S:\n");
        disp(S, 4);
        fprintf("V:\n");
        disp(V, 4);
    }
}
