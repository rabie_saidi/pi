package pi.optimisation;

/**
 * @author Rabie Saidi
 */
public class ExampleData {
    public static double[][] data = {
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {1, 1, 1, 0.94, 0.85, 1, 0.94, 0.85, 0.93, 0.85, 0.8, 0.94, 0.85, 0.8, 0.8, 0.8,0.86, 0.71, 0.85, 0.73, 0.42, 0.26, 0.19, 0, 0, 0, 0, 0, 0.94, 0, 0},
            {1, 0.66, 1, 0.83, 0.57, 0.66, 0.62, 0.57, 0.83, 0.57, 0.53, 0.62, 0.57, 0.53, 0.53, 0.53, 0.78, 0.62, 0.78, 0.69, 0.48, 0.17, 0.13, 0.21, 0.16, 0.16, 0.10, 0.06, 0.69, 0, 0},
            {1, 0.14, 0.18, 1, 0.12, 0.14, 0.14, 0.12, 0.18, 0.12, 0.12, 0.14, 0.12, 0.12, 0.12, 0.12, 0.41, 0.19, 0.39, 0.32, 0.28, 0.24, 0.03, 0.15, 0.04, 0.04, 0.02, 0.11, 0.58, 0.06, 0.05},
            {1, 0.79, 0.79, 0.74, 1, 0.79, 0.74, 0.79, 0.74, 0.79, 0.74, 0.74, 0.79, 0.74, 0.74, 0.74, 1.00, 0.53, 1.00, 0.67, 0.38, 0.24, 0.18, 0, 0, 0, 0, 0, 0.76, 0, 0},
            {1, 1, 1, 0.92, 0.85, 1, 0.94, 0.85, 0.94, 0.85, 0.8, 0.94, 0.85, 0.8, 0.8, 0.8, 0.86, 0.71, 0.85, 0.73, 0.42, 0.26, 0.19, 0, 0, 0, 0, 0, 0.94, 0, 0},
            {1, 1, 1, 1, 0.85, 1, 1, 0.85, 1, 0.85, 0.85, 1, 0.85, 0.85,  0.85, 0.85, 0.85, 0.73, 0.85, 0.75, 0.44, 0.28, 0.2, 0, 0, 0, 0, 0, 0.96, 0, 0},
            {1, 1, 1, 0.94, 1, 1, 0.94, 1, 0.94, 1, 0.94, 0.94, 1, 0.94, 0.94, 0.94, 1.00, 0.66, 1.00, 0.83, 0.48, 0.30, 0.23, 0, 0, 0, 0, 0, 0.93, 0, 0},
            {1, 0.75, 1, 1, 0.64, 0.75, 0.75, 0.64, 1, 0.64, 0.64, 0.75, 0.64, 0.64, 0.64, 0.64, 0.89, 0.74, 0.89, 0.81, 0.57, 0.21, 0.15, 0.25, 0.2,  0.2,  0.12, 0.07, 0.97, 0, 0},
            {1, 1, 1, 0.94, 1, 1, 0.94, 1, 0.94, 1, 0.94, 0.94, 1, 0.94, 0.94, 0.94, 0.71, 0.66, 1.00, 0.83, 0.48, 0.30, 0.23, 0, 0, 0, 0, 0, 0.93, 0, 0},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1.00, 0.68, 1.00, 0.86, 0.50, 0.33, 0.24, 0, 0, 0, 0, 0, 0.95, 0, 0},
            {1, 1, 1, 1, 0.85, 1, 1, 0.85, 1, 0.85, 0.85, 1, 0.85, 0.85, 0.85, 0.85, 0.85, 0.73, 0.85, 0.75, 0.44, 0.28, 0.20, 0, 0, 0, 0, 0, 0.96, 0, 0},
            {1, 1, 1, 0.94, 1, 1, 0.94, 1, 0.94, 1, 0.94, 0.94, 1, 0.94, 0.94, 0.94, 1.00, 0.66, 1.00, 0.83, 0.48, 0.30, 0.23, 0, 0, 0, 0, 0, 0.93, 0, 0},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1.00, 0.68, 1.00, 0.86, 0.50, 0.33, 0.24, 0, 0, 0, 0, 0, 0.95, 0, 0},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1.00, 0.68, 1.00, 0.86, 0.50, 0.33, 0.24, 0, 0, 0, 0, 0, 0.95, 0, 0},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1.00, 0.68, 1.00, 0.86, 0.50, 0.33, 0.24, 0, 0, 0, 0, 0, 0.95, 0, 0}
    };

    public static double[][] data2 = {
            {1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {1, 1, 1, 0.94, 0.85, 0.86, 0.71, 0.85, 0.73, 0.42, 0.26, 0.19, 0, 0, 0, 0, 0, 0.94, 0, 0},
            {1, 0.66, 1, 0.83, 0.57, 0.78, 0.62, 0.78, 0.69, 0.48, 0.17, 0.13, 0.21, 0.16, 0.16, 0.10, 0.06, 0.69, 0, 0},
            {1, 0.14, 0.18, 1, 0.12, 0.41, 0.19, 0.39, 0.32, 0.28, 0.24, 0.03, 0.15, 0.04, 0.04, 0.02, 0.11, 0.58, 0.06, 0.05},
            {1, 0.79, 0.79, 0.74, 1, 1.00, 0.53, 1.00, 0.67, 0.38, 0.24, 0.18, 0, 0, 0, 0, 0, 0.76, 0, 0},

    };

    public static double[][] data3 = {
            {2,6,8,2,3},
            {15,2,0,99,3},
            {8,0,6,0,4}
    };
}
