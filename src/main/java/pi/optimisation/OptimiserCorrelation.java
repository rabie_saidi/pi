package pi.optimisation;

import la.matrix.Matrix;
import pi.core.prediction.PredictionSet;

import static ml.utils.Printer.fprintf;
import static ml.utils.Time.tic;
import static ml.utils.Time.toc;

/**
 * @author Rabie Saidi
 */
public class OptimiserCorrelation implements Optimiser{
    private  OptimisationMatrix matrix;

    public OptimiserCorrelation(OptimisationMatrix matrix) {
        this.matrix = matrix;
    }

    @Override
    public PredictionSet predict() {
        //tic();
        double[] arrayToCorrelate = extractAttributeArray(0);
        int bestCorrelationIndex = 1;
        double bestCorrelationScore = 0;
        if(matrix.getAtrributeCount() > 1) {
            for (int i = 1; i < matrix.getAtrributeCount(); i++) {
                double[] attributeArray = extractAttributeArray(i);
                double score = cosineSimilarity(arrayToCorrelate, attributeArray);
                if (score > bestCorrelationScore) {
                    bestCorrelationScore = score;
                    bestCorrelationIndex = i;
                }
            }
        }
        matrix.setSolution(solution(bestCorrelationIndex));
        PredictionSet predictionSet = matrix.getPredictionSet();
        //fprintf("Correlation - Elapsed time: %.2f seconds.%n", toc());
        return predictionSet;
    }

    private Matrix solution(int bestCorrelationIndex) {
        Matrix solution = matrix.getMatrix().copy();
        for(int i=matrix.getAtrributeCount() ; i<matrix.getColumnDimension() ; i++){
            solution.setEntry(0, i, solution.getEntry(bestCorrelationIndex, i));
        }
        return solution;
    }

    private double[] extractAttributeArray(int rowIndex){
        double[] attributeArray = new double[1+matrix.getAtrributeCount()];
        for(int i=0 ; i<=matrix.getAtrributeCount() ; i++){
            attributeArray[i] = matrix.getMatrix().getEntry(rowIndex, i);
        }
        return attributeArray;
    }

    @Override
    public OptimisationMatrix getMatrix() {
        return matrix;
    }

    private static double cosineSimilarity(double[] vectorA, double[] vectorB) {
        double dotProduct = 0.0;
        double normA = 0.0;
        double normB = 0.0;
        for (int i = 0; i < vectorA.length; i++) {
            dotProduct += vectorA[i] * vectorB[i];
            normA += Math.pow(vectorA[i], 2);
            normB += Math.pow(vectorB[i], 2);
        }
        return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
    }
}
