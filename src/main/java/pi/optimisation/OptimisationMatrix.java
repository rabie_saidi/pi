package pi.optimisation;

import la.matrix.Matrix;
import pi.core.prediction.Prediction;
import pi.core.prediction.PredictionSet;
import scala.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rabie Saidi
 */
public class OptimisationMatrix implements Serializable{
    private String entry;
    private Matrix matrix;
    private Matrix solution;
    private List<String> annotations;
    private List<String> attributes;

    public OptimisationMatrix(String entry, Matrix matrix, List<String> annotations, List<String> attributes) {
        this.entry = entry;
        this.matrix = matrix;
        this.annotations = annotations;
        this.attributes = attributes;
    }

    public String getEntry() {
        return entry;
    }

    public Matrix getMatrix() {
        return matrix;
    }

    public List<String> getAnnotations() {
        return annotations;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public boolean isTall() {
        return matrix.getColumnDimension() <= matrix.getRowDimension();
    }

    public int getAnnotationCount(){
        return annotations.size();
    }

    public int getAtrributeCount(){
        return attributes.size();
    }

    public int getColumnDimension(){
        return matrix.getColumnDimension();
    }

    public int getRowDiemension(){
        return matrix.getRowDimension();
    }

    public Matrix getSolution() {
        return solution;
    }

    public void setSolution(Matrix solution){
        this.solution = solution;
    }

    public PredictionSet getPredictionSet(){
        List<Prediction> predictions = new ArrayList<>();
        int start = getColumnDimension() - getAnnotationCount();
        int end = getColumnDimension();
        for(int i=start; i<end; i++){
            double score = solution.getEntry(0, i);
            if(score >= 0.8){
                Prediction prediction = new Prediction(entry,
                        annotations.get(i-start),
                        solution.getRowVector(0).toString(),
                        score);
                predictions.add(prediction);
            }
        }
        return new PredictionSet(predictions);
    }
}
