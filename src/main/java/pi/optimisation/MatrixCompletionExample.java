package pi.optimisation;


import la.matrix.Matrix;
import ml.recovery.MatrixCompletion;

import static ml.utils.Matlab.*;
import static ml.utils.Printer.disp;
import static ml.utils.Printer.fprintf;
import static ml.utils.Time.tic;
import static ml.utils.Time.toc;

/**
 * @author Rabie Saidi
 */
public class MatrixCompletionExample {
    public static void main(String[] args) {
        int numSig = 4;
        Matrix d = new la.matrix.DenseMatrix(ExampleData.data2);
        Matrix omega = ones(size(d));
        for(int i=numSig+1 ; i<d.getColumnDimension() ; i++){
            omega.setEntry(0, i, 0);
        }
        //omega.setEntry(10, 20, 0);
        //omega.setEntry(8, 27, 0);
        // Run matrix completion
        MatrixCompletion matrixCompletion = new MatrixCompletion();
        matrixCompletion.feedData(d.transpose());
        matrixCompletion.feedIndices(omega.transpose());
        tic();
        matrixCompletion.run();
        fprintf("Elapsed time: %.2f seconds.%n", toc());

        // Output
        Matrix a_hat = matrixCompletion.GetLowRankEstimation().transpose();
        fprintf("A^:\n");
        disp(a_hat, 2);
        fprintf("D:\n");
        disp(d, 2);
        fprintf("Omega:\n");
        disp(omega, 2);
        fprintf("rank(D): %d\n", rank(d));
        fprintf("rank(A^): %d\n", rank(a_hat));
        //fprintf("||D - A^||_F: %.4f\n", norm(d.minus(a_hat), "fro"));
    }
}
