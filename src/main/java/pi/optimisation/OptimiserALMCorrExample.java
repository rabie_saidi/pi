package pi.optimisation;

import la.matrix.Matrix;
import pi.core.prediction.PredictionSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Rabie Saidi
 */
public class OptimiserALMCorrExample {
    public static void main(String[] args){
        String entry = "Tr_entry";
        Matrix data = new la.matrix.DenseMatrix(ExampleData.data2);
        List<String> annotations = Arrays.asList("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14", "A15");;
        List<String> attributes = Arrays.asList("Sig1", "Sig2", "Sig3", "Sig4");

        OptimisationMatrix matrix = new OptimisationMatrix(entry, data, annotations, attributes);
        OptimisationMatrix matrix1 = new OptimisationMatrix(entry, data, annotations, attributes);
        OptimisationMatrix matrix2 = new OptimisationMatrix(entry, data, annotations, attributes);


//        Optimiser optimiser = new OptimiserALM(matrix);
//        PredictionSet predictionSet = optimiser.predict();
//        System.out.println("ALM - Predictions: \n" + predictionSet);
//
//
//        Optimiser optimiser1 = new OptimiserCorrelation(matrix1);
//        PredictionSet predictionSet1 = optimiser1.predict();
//        System.out.println("Correlation - Predictions: \n" + predictionSet1);


        Optimiser optimiser2 = new OptimiserBase(matrix2, 20, TimeUnit.MILLISECONDS);
        PredictionSet predictionSet2 = optimiser2.predict();
        System.out.println("Base - Predictions: \n" + predictionSet2);
    }
}
