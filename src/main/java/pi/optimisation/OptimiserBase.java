package pi.optimisation;

import pi.core.prediction.PredictionSet;

import java.util.concurrent.*;

/**
 * @author Rabie Saidi
 */
public class OptimiserBase implements Optimiser{
    private OptimisationMatrix matrix;
    private long timeout;
    private TimeUnit timeUnit;

    public OptimiserBase(OptimisationMatrix matrix, long timeout, TimeUnit timeUnit) {
        this.matrix = matrix;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
    }

    @Override
    public PredictionSet predict() {
        final ExecutorService executor = Executors.newSingleThreadExecutor();
        final Future<PredictionSet> future = executor.submit(new OptimiserALM(matrix));
        executor.shutdown();
        try {
            return future.get(timeout, timeUnit);
        } catch (TimeoutException e) {
            future.cancel(true);
            executor.shutdownNow();
            OptimisationMatrix matrix2 = new OptimisationMatrix(matrix.getEntry(), matrix.getMatrix(),
                    matrix.getAnnotations(), matrix.getAttributes());
            return  new OptimiserCorrelation(matrix2).predict();
        }catch (ExecutionException | InterruptedException ignored) {
            executor.shutdownNow();
            return new PredictionSet();
        }
    }

    @Override
    public OptimisationMatrix getMatrix() {
        return matrix;
    }
}
