package pi.optimisation;

import la.matrix.Matrix;
import ml.recovery.MatrixCompletion;
import pi.core.prediction.Prediction;
import pi.core.prediction.PredictionSet;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static ml.utils.Matlab.ones;
import static ml.utils.Matlab.rank;
import static ml.utils.Matlab.size;
import static ml.utils.Printer.disp;
import static ml.utils.Printer.fprintf;
import static ml.utils.Time.tic;
import static ml.utils.Time.toc;

/**
 * @author Rabie Saidi
 */
public class OptimiserALM implements Optimiser, Callable<PredictionSet> {
    private OptimisationMatrix matrix;

    public OptimiserALM(OptimisationMatrix matrix) {
        this.matrix = matrix;
    }

    @Override
    public PredictionSet predict() {
        tic();
        MatrixCompletion matrixCompletion = new MatrixCompletion();
        matrixCompletion.feedData(data(matrix));
        matrixCompletion.feedIndices(omega(matrix));
        matrixCompletion.run();
        matrix.setSolution(adequateMatrix(matrixCompletion.GetLowRankEstimation()));
        PredictionSet predictionSet = matrix.getPredictionSet();
        fprintf("ALM - Elapsed time: %.2f seconds.%n", toc());
        return predictionSet;
    }

    private Matrix data(OptimisationMatrix matrix){
        return adequateMatrix(matrix.getMatrix());
    }

    private Matrix adequateMatrix(Matrix data){
        return matrix.isTall() ? data : data.transpose();
    }

    private Matrix omega(OptimisationMatrix matrix){
        Matrix omega = ones(size(matrix.getMatrix()));
        int start = matrix.getColumnDimension() - matrix.getAnnotationCount();
        int end = matrix.getColumnDimension();
        for(int i=start ; i<end ; i++){
            omega.setEntry(0, i, 0);
        }
        return matrix.isTall() ? omega : omega.transpose();
    }


    @Override
    public OptimisationMatrix getMatrix() {
        return matrix;
    }

    @Override
    public PredictionSet call() throws Exception {
        PredictionSet predictionSet = predict();
        fprintf("ALM - Elapsed time: %.2f seconds.%n", toc());
        return predictionSet;
    }
}
