package pi.app;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.graphx.Graph;
import org.apache.spark.graphx.VertexRDD;
import org.slf4j.LoggerFactory;
import pi.core.aggregate.MergeV;
import pi.core.aggregate.SendV;
import pi.core.aggregate.SendV2;
import pi.core.edge.EdgeProperty;
import pi.core.prediction.PredictionSet;
import pi.core.vertex.Vertex;
import pi.util.Util;
import scala.Serializable;

import java.io.File;

/**
 * @author Rabie Saidi
 */
public class Run implements Serializable {
    static private org.slf4j.Logger log = LoggerFactory.getLogger(Run.class.getName());
    public static void main(String[] args) {
        if (args.length != 4) {
            System.out.println("There must be 4 parameters:" +
                    "\n-UniprotInput(UniProt entries)" +
                    "\n-Rate of entries to be taken from the input file (e.g., 0.1)"+
                    "\n-Output" +
                    "\n-Running mode: choose either local or cluster");
        } else {
            //Read command line parameters
            String uniprotInput = args[0];
            double rate = Double.parseDouble(args[1]);
            String output = args[2];
            String mode = args[3];
            //Set up the Spark context
            JavaSparkContext sc = Config.getSparkContext(mode);
            Util util = new Util();

            Graph<Vertex, EdgeProperty> graph = util.createGraph(sc, uniprotInput, rate);
            //util.saveTripletsAsTextFile(graph.triplets(),output + File.separator + "graph");
            VertexRDD<Vertex> vertexRDD = util.aggregateMessages(graph, new SendV(), new MergeV());
            //util.saveVerticesAsTextFile(vertexRDD, output + File.separator + "1");
            Graph<Vertex, EdgeProperty> graph2 = util.createGraph(vertexRDD, graph.edges());
            VertexRDD<Vertex> vertexRDD2 = util.aggregateMessages(graph2, new SendV2(), new MergeV());
            //util.saveVerticesAsTextFile(vertexRDD2, output + File.separator + "2");
            JavaRDD<PredictionSet> predictions = util.getPredictions(vertexRDD2);
            util.savePredictionsAsTextFile(predictions, output + File.separator + "predictions");
            JavaRDD<String> plotData = util.getPlotData(predictions);
            plotData.repartition(1).saveAsTextFile(output + File.separator + "plot");

        }
    }
}

