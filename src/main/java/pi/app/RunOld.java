//package pi.app;
//
//import org.apache.commons.collections.comparators.ComparableComparator;
//import org.apache.spark.api.java.JavaRDD;
//import org.apache.spark.api.java.JavaSparkContext;
//import org.apache.spark.api.java.function.Function;
//import org.apache.spark.graphx.*;
//import org.apache.spark.storage.StorageLevel;
//import org.slf4j.LoggerFactory;
//import pi.core.edge.EdgeProperty;
//import pi.core.aggregate.MergeV;
//import pi.core.aggregate.SendV;
//import pi.core.aggregate.SendV2;
//import pi.core.signal.AnnotationMap;
//import pi.app.old.vertexold.*;
//import scala.Serializable;
//import scala.Tuple2;
//import scala.math.Ordering;
//import scala.math.Ordering$;
//import scala.reflect.ClassTag;
//import scala.reflect.ClassTag$;
//
//import java.util.List;
//
///**
// * @author Rabie Saidi
// */
//public class RunOld implements Serializable {
//    static private org.slf4j.Logger log = LoggerFactory.getLogger(RunOld.class.getName());
//    public static void main(String[] args) {
//        if (args.length < 7) {
//            System.out.println("There must be 7 parameters:" +
//                    "\n-UniprotInput(UniProt entries)" +
//                    "\n-UniprotList(List of protein accessions)" +
//                    "\n-InterproInput(InterPro accessions)" +
//                    "\n-Unp2IprInput(Mapping between InterPro and UniProt)" +
//                    "\n-Output" +
//                    "\n-Rate, print the percentage"+
//                    "\n-Running mode: choose either local or cluster");
//        } else {
//
//            String uniprotInput = args[0];
//            String uniprotList = args[1];
//            String interproInput = args[2];
//            String unp2IprInput = args[3];
//            String output = args[4];
//            double rate = Double.parseDouble(args[5]);
//            String mode = args[6];
//            //Set up the Spark context
//            JavaSparkContext sc = Config.getSparkContext(mode);
//            //
//            sc.hadoopConfiguration().set("textinputformat.record.delimiter", Config.unpDelimiter);
//            JavaRDD<String> entryTexts = sc.textFile(uniprotInput);
//            JavaRDD<String> entryTextsSample = rate == 1 ? entryTexts : entryTexts.sample(false, rate);
//            JavaRDD<Vertex> unpVertices = entryTextsSample.map(v1 -> new VertexMaker().createVertexProtein(v1));
//            //This is just to force Spark to execute before changing the delimiter
//            unpVertices.first();
//            sc.hadoopConfiguration().set("textinputformat.record.delimiter", Config.iprDelimiter);
//
//            JavaRDD<String> knownEntryJavaRDD = sc.textFile(uniprotList);
//            List knownInstances = knownEntryJavaRDD.collect();
//
//            JavaRDD<String> iprTexts = sc.textFile(interproInput);
//            JavaRDD<Vertex> iprVertices = iprTexts.map(new Function<String, Vertex>() {
//                @Override
//                public Vertex call(String s) throws Exception {
//                    String[] tokens = s.split("\\t");
//                    //return new Attribute(tokens[0], tokens[1]);
//                    Vertex attribute = new VertexImpl(tokens[0], VertexType.ATTRIBUTE);
////                String annotation = String.valueOf((char) (new Random().nextInt(26) + (byte) 'a'));
////                attribute.addAnnotation(annotation);
//                    return attribute;
//                }
//            });
//            //Union the two types of vertices
//            JavaRDD<Vertex> unionJavaRDD = unpVertices.union(iprVertices);
//            //unionJavaRDD.repartition(1).saveAsTextFile(output);
//
//            JavaRDD<Tuple2<Object, Vertex>> vertexJavaRDD = unionJavaRDD.map(new Function<Vertex, Tuple2<Object, Vertex>>() {
//                @Override
//                public Tuple2<Object, Vertex> call(Vertex vertexold) throws Exception {
//                    long id = Long.valueOf(vertexold.getAccession().hashCode());
//                    return new Tuple2<>(id, vertexold);
//                }
//            });
//
//            ClassTag<Vertex> vertexClassTag = ClassTag$.MODULE$.apply(Vertex.class);
//            VertexRDD<Vertex> vertexRDD = VertexRDD.apply(vertexJavaRDD.rdd(), vertexClassTag);
//
//            Ordering<Tuple2<Object, Vertex>> orderingV = Ordering$.MODULE$.comparatorToOrdering(new ComparableComparator());
//            //vertexRDD.repartition(1, ordering).saveAsTextFile(output);
//
//            JavaRDD<String> unp2IprTexts = sc.textFile(unp2IprInput).filter(new Function<String, Boolean>() {
//                @Override
//                public Boolean call(String s) throws Exception {
//                    return !s.startsWith("#");
//                }
//            });
//            JavaRDD<Edge<EdgeProperty>> edgeJavaRDD = unp2IprTexts.map(new Function<String, Edge<EdgeProperty>>() {
//                @Override
//                public Edge<EdgeProperty> call(String s) throws Exception {
//                    String[] tokens = s.split("\\t");
//                    //O40976
//                    String vertexName1 = tokens[0];
//                    String vertexName2 = tokens[2];
//                    long vertexId1 = (long) tokens[0].hashCode();
//                    long vertexId2 = (long) tokens[2].hashCode();
//                    if (!knownInstances.contains(vertexName1)) {
////                if(vertexName1.equals("O40976")
////                        || vertexName1.equals("P19811")
////                        || vertexName1.equals("P10447")) {
//                        return new Edge<>(vertexId2, vertexId1, new EdgeProperty(vertexName2, vertexName1));
//                    }
//                    return new Edge<>(vertexId1, vertexId2, new EdgeProperty(vertexName1, vertexName2));
//                }
//            });
//
//            ClassTag<EdgeProperty> edgeClassTag = ClassTag$.MODULE$.apply(EdgeProperty.class);
//            EdgeRDD<EdgeProperty> edgeRDD = EdgeRDD.fromEdges(edgeJavaRDD.rdd(), edgeClassTag, vertexClassTag);
//
//            Ordering<Edge<EdgeProperty>> orderingE = Ordering$.MODULE$.comparatorToOrdering(new ComparableComparator());
//            //edgeRDD.repartition(1, ordering).saveAsTextFile(output);
//
//            Vertex defaultVertex = new DefaultVertex();
//
//            Graph<Vertex, EdgeProperty> graph = Graph.apply(vertexRDD, edgeRDD, defaultVertex,
//                    StorageLevel.MEMORY_ONLY(), StorageLevel.MEMORY_ONLY(),
//                    vertexClassTag, edgeClassTag);
//            Ordering<EdgeTriplet<Vertex, EdgeProperty>> ord = Ordering$.MODULE$.comparatorToOrdering(new ComparableComparator());
//            //graph.triplets().repartition(1, ord).saveAsTextFile(output);
//
//            GraphOps<Vertex, EdgeProperty> graphOps = new GraphOps<>(graph, vertexClassTag, edgeClassTag);
//            ClassTag<AnnotationMap> classTagA = ClassTag$.MODULE$.apply(AnnotationMap.class);
//
//
//            VertexRDD<Vertex> vertexRDD1 = graph.aggregateMessages(
//                    new SendV(),
//                    new MergeV(),
//                    new TripletFields(true, true, true),
//                    vertexClassTag
//            );
//            vertexRDD1.first();
//            //vertexRDD1.repartition(1, orderingV).saveAsTextFile(output);
//
//            Graph<Vertex, EdgeProperty> graph2 = Graph.apply(vertexRDD1, edgeRDD, defaultVertex,
//                    StorageLevel.MEMORY_ONLY(), StorageLevel.MEMORY_ONLY(),
//                    vertexClassTag, edgeClassTag);
//
//            //tripletRDD.repartition(1, ord).saveAsTextFile(output);
//            VertexRDD<Vertex> vertexRDD2 = graph2.aggregateMessages(
//                    new SendV2(),
//                    new MergeV(),
//                    new TripletFields(true, true, true),
//                    vertexClassTag
//            );
//            vertexRDD2.repartition(1, orderingV).saveAsTextFile(output);
//        }
//    }
//}
//
