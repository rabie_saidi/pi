package pi.app;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * @author Rabie Saidi
 */
public class Config {
    static String unpDelimiter = "//\n";
    static String iprDelimiter = "\n";
    public static JavaSparkContext getSparkContext(String mode){
        SparkConf conf = new SparkConf();
        conf.setAppName("pi");
        if (mode.equals("local")) {
            conf.setMaster("local[*]");
            System.setProperty("hadoop.home.dir", "C:\\hadoop");
        }
        conf.set("spark.hadoop.validateOutputSpecs", "false");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer"); // kryo is much faster
        conf.set("spark.akka.frameSize", "500"); // workers should be able to send bigger messages
        conf.set("spark.akka.askTimeout", "30"); // high CPU/IO load
        System.setProperty("spark.kryoserializer.buffer.mb", "256"); // I serialize bigger objects
        System.setProperty("spark.mesos.coarse", "true"); // link provided

        JavaSparkContext sc = new JavaSparkContext(conf);
        sc.hadoopConfiguration().set("textinputformat.record.delimiter", Config.unpDelimiter);//Set delimiter to read UniProt entries
        return sc;
    }
}
