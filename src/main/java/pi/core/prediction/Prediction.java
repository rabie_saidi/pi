package pi.core.prediction;

/**
 * @author Rabie Saidi
 */
public class Prediction {
    private String entry;
    private String annotation;
    private String rule;
    private double score;

    public Prediction(String entry, String annotation, String rule, double score) {
        this.entry = entry;
        this.annotation = annotation;
        this.rule = rule;
        this.score = score;
    }

    public String getEntry() {
        return entry;
    }

    public String getRule() {
        return rule;
    }

    public String toUniProtFormat(){
        return "PI" + "_" + rule.hashCode() + "\t" +
                "PROTEINNAME_RECOMMENDED_EC" + "\t" +  annotation + "\t" +
                entry + "\t" +
                "?\t" +
                "?\t" +
                0 + "\t" +
                0 + "\t" +
                "unknown" + "\t" +
                "PREDICTED" + "\t" +
                "PI" + ":+:" + rule.hashCode() + ";";
    }

    public double getScore() {
        return score;
    }
}
