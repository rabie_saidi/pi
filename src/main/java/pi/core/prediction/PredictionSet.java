package pi.core.prediction;

import scala.Serializable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Rabie Saidi
 */
public class PredictionSet implements Serializable {
    private List<String> predictions = new ArrayList<>();

    public PredictionSet() {
    }

    public PredictionSet(List<Prediction> predictions) {
        //this.predictions = new HashSet<>();
        this.predictions.addAll(predictions.stream().map(Prediction::toUniProtFormat).collect(Collectors.toList()));
    }

    public List<String> getPredictions() {
        return predictions;
    }

    public boolean isEmpty() {
        return predictions.isEmpty();
    }

    public void addAll(PredictionSet predictionSet) {
        this.predictions.addAll(predictionSet.predictions);
    }

    public int size() {
        return predictions.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int count = 0;
        for (String prediction : predictions) {
            count++;
            if (count != predictions.size()) {
                sb.append(prediction).append("\n");
            } else {
                sb.append(prediction);
            }
        }
        return sb.toString();
    }
}
