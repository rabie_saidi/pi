package pi.core.vertex;

import pi.core.signal.AnnotationMatrix;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Rabie SAIDI
 */
public class VertexProtein extends VertexAbstract {
    private VertexProteinType vertexProteinType;
    private SortedSet<String> signatures = new TreeSet<>();
    private SortedSet<String> taxa = new TreeSet<>();
    private AnnotationMatrix annotationMatrix = new AnnotationMatrix();

    public VertexProtein(String id, VertexProteinType vertexProteinType) {
        super(id);
        this.vertexProteinType = vertexProteinType;
    }

    public VertexProtein(String id, VertexProteinType vertexProteinType, SortedSet<String> signatures, SortedSet<String> taxa) {
        super(id);
        this.vertexProteinType = vertexProteinType;
        this.signatures.addAll(signatures);
        this.taxa.addAll(taxa);
    }

    @Override
    public VertexType getVertexType(){
        return VertexType.INSTANCE;
    }

    public VertexProteinType getVertexProteinType() {
        return vertexProteinType;
    }

    public SortedSet<String> getSignatures(){
        return signatures;
    }

    public AnnotationMatrix getAnnotationMatrix() {
        return annotationMatrix;
    }

//    public void addAttributeAnnotation(String attribute, String annotation){
//        annotationMatrix.addAttributeAnnotation(attribute, annotation);
//    }

//    public void merge (AnnotationMatrix annotationMatrix){
//        this.annotationMatrix.merge(annotationMatrix);
//    }

    @Override
    public String toString() {
        return "VertexProtein{" +
                "accession='" + super.getAccession() + '\'' +
                ", vertexProteinType=" + vertexProteinType +
                ", signatures=" + signatures +
                ", annotationMap=" + super.getAnnotationMap() +
                ", annotationMatrix=" + annotationMatrix +
                ", from=" + super.getFrom() +
                '}';
    }

    public void addToAnnotationMatrix(VertexAttribute attribute) {
        annotationMatrix.addToAnnotationMatrix(attribute);
    }

    public void addAnnotation (String annotation){
        super.getAnnotationMap().addAnnotation(annotation, super.getAccession());
    }

}
