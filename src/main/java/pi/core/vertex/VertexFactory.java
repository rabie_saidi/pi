package pi.core.vertex;

import scala.Serializable;
import scala.Tuple2;
import uk.ac.ebi.kraken.interfaces.uniprot.DatabaseType;
import uk.ac.ebi.kraken.interfaces.uniprot.UniProtEntry;
import uk.ac.ebi.kraken.interfaces.uniprot.UniProtEntryType;
import uk.ac.ebi.kraken.interfaces.uniprot.description.Field;
import uk.ac.ebi.kraken.interfaces.uniprot.description.FieldType;
import uk.ac.ebi.kraken.model.factories.DefaultUniProtFactory;
import uk.ac.ebi.kraken.parser.UniProtParser;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Rabie SAIDI
 */
public class VertexFactory implements Serializable{
    private static final String EOL_WINDOWS = "\r\n";
    private static final String EOL_UNIX = "\n";
    private static final String FF_ENTRY_DELIMITER = "//";
    public Vertex createVertexProtein(String entryText) throws Exception {
        String entryString = entryText.trim() + EOL_UNIX + FF_ENTRY_DELIMITER;
        UniProtEntry uniProtEntry = UniProtParser.parse(entryString, DefaultUniProtFactory.getInstance());
        String entryId = uniProtEntry.getPrimaryUniProtAccession().getValue();
        SortedSet<String> iprs = new TreeSet<>();
        uniProtEntry.getDatabaseCrossReferences(DatabaseType.INTERPRO).forEach(
                ipr -> iprs.add(ipr.getPrimaryId().getValue())
        );
        SortedSet<String> taxa = new TreeSet<>();
        uniProtEntry.getTaxonomy().forEach(taxon ->taxa.add(taxon.getValue()));

        VertexProtein vertex;
        if (uniProtEntry.getType() == UniProtEntryType.SWISSPROT){
            vertex = new VertexProtein(entryId, VertexProteinType.KNOWN_INSTANCE, iprs, taxa);
//            List<Field> fields = new ArrayList<>();
//            //Add recommended
//            fields.addAll(uniProtEntry.getProteinDescription().getRecommendedName().getFieldsByType(FieldType.EC));
////                    //Add alternative
////                    uniProtEntry.getProteinDescription().getAlternativeNames().forEach(
////                            name -> fields.addAll(name.getFieldsByType(FieldType.EC)));
//            //Add contained
//            uniProtEntry.getProteinDescription().getContains().forEach(
//                    section -> section.getNames().forEach(
//                            name -> fields.addAll(name.getFieldsByType(FieldType.EC)))
//            );
//            //Add included
//            uniProtEntry.getProteinDescription().getIncludes().forEach(
//                    section -> section.getNames().forEach(
//                            name -> fields.addAll(name.getFieldsByType(FieldType.EC)))
//            );
            List<String> fields = new ArrayList<>();
            fields.addAll(uniProtEntry.getProteinDescription().getEcNumbers());
            if(!fields.isEmpty()){
                //fields.forEach(field -> vertex.addAnnotation(field.getValue()));
                fields.forEach(vertex::addAnnotation);
            }
        }
        else{
            vertex = new VertexProtein(entryId, VertexProteinType.UNKNOWN_INSTANCE, iprs, taxa);
        }

        return vertex;
    }

    public Vertex createVertexAttribute(Tuple2<String, Iterable<Vertex>> pair) throws Exception {
        return new VertexAttribute(pair._1, pair._2);
    }

}
