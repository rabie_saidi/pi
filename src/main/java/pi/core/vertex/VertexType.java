package pi.core.vertex;

/**
 * @author Rabie Saidi
 */
public enum VertexType {
    INSTANCE,
    ATTRIBUTE,
    NONE
}
