package pi.core.vertex;

import pi.core.signal.AnnotationMap;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Rabie SAIDI
 */
abstract class VertexAbstract implements Vertex{
    private String accession;
    private double weight;
    private VertexType vertexType;
    private AnnotationMap annotationMap = new AnnotationMap();
    private SortedSet<String> from = new TreeSet<>();

    VertexAbstract(String accession) {
        this.accession = accession;
    }

    @Override
    public String getAccession() {
        return accession;
    }

    @Override
    public VertexType getVertexType() {
        return vertexType;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public AnnotationMap getAnnotationMap() {
        return annotationMap;
    }

    @Override
    public SortedSet<String> getFrom(){
        return from;
    }

    @Override
    public void merge (AnnotationMap annotationMap){
        this.annotationMap.merge(annotationMap);
    }

    @Override
    public void addToFrom(String source){
        from.add(source);
    }

    @Override
    public void addToFrom(SortedSet<String> source){
        from.addAll(source);
    }

    @Override
    public String toString() {
        return "VertexAbstract{" +
                "accession='" + accession + '\'' +
                ", weight=" + weight +
                ", vertexType=" + vertexType +
                ", annotationMap=" + annotationMap +
                ", from=" + from +
                '}';
    }
}
