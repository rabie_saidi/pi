package pi.core.vertex;

/**
 * @author Rabie Saidi
 */
public enum VertexProteinType {
    KNOWN_INSTANCE,
    UNKNOWN_INSTANCE,
}
