package pi.core.vertex;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Rabie SAIDI
 */
public class VertexAttribute extends VertexAbstract {
    private int size;
    private SortedSet<String> instances = new TreeSet<>();

    public VertexAttribute(String id, Iterable<Vertex> instances) {
        super(id);
        instances.forEach(vertex -> {
            VertexProtein protein = (VertexProtein) vertex;
            if(protein.getVertexProteinType() == VertexProteinType.KNOWN_INSTANCE)
                this.instances.add(vertex.getAccession());
        });
        size = this.instances.size();
    }

    public VertexAttribute(String id, int size) {
        super(id);
        this.size = size;
    }

    public VertexAttribute(String id) {
        super(id);
    }

    public int getSize() {
        return size;
    }

    public SortedSet<String> getInstances() {
        return instances;
    }

    @Override
    public VertexType getVertexType(){
        return VertexType.ATTRIBUTE;
    }

    @Override
    public String toString() {
        return "Vertexattribute{" +
                "accession='" + super.getAccession() + '\'' +
                ", size=" + size +
                ", vertexType=" + getVertexType() +
                ", annotationMap=" + super.getAnnotationMap() +
                ", from=" + super.getFrom() +
                '}';
    }

}
