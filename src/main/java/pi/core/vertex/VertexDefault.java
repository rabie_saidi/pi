package pi.core.vertex;

import pi.core.signal.AnnotationMap;

import java.util.SortedSet;

/**
 * @author Rabie Saidi
 */
public class VertexDefault implements Vertex {
    @Override
    public String getAccession() {
        return "DEFAULT_VERTEX";
    }

    @Override
    public VertexType getVertexType() {
        return VertexType.NONE;
    }

    @Override
    public double getWeight() {
        return 0;
    }

    @Override
    public AnnotationMap getAnnotationMap() {
        return null;
    }

    @Override
    public void merge(AnnotationMap map) {

    }

    @Override
    public void addToFrom(SortedSet<String> source) {

    }

    @Override
    public void addToFrom(String source) {

    }

    @Override
    public SortedSet<String> getFrom() {
        return null;
    }

    @Override
    public String toString() {
        return "DEFAULT_VERTEX";
    }
}
