package pi.core.vertex;

import pi.core.signal.AnnotationMap;
import scala.Serializable;

import java.util.SortedSet;

/**
 * @author Rabie SAIDI
 */
public interface Vertex extends Serializable{
    String getAccession();
    VertexType getVertexType();
    double getWeight();
    AnnotationMap getAnnotationMap();
    void merge (AnnotationMap map);
    //void addAnnotation (String annotation);
    void addToFrom(SortedSet<String> source);
    void addToFrom(String source);
    SortedSet<String> getFrom();
}
