package pi.core.signal;

import pi.core.vertex.VertexAttribute;
import pi.util.CombinationMaker;
import scala.Serializable;
import scala.Tuple2;

import java.util.*;
import java.util.stream.Collectors;


/**
 * @author Rabie Saidi
 */
public class AnnotationMatrix  implements Serializable {
    private Map<String, Map<String, SortedSet<String>>> matrix = new TreeMap<>();
    private Map<String, SortedSet<String>> attributes = new TreeMap<>();

    public Map<String, Map<String, SortedSet<String>>> getMatrix() {
        return matrix;
    }

    public Map<String, SortedSet<String>> getAttributes() {
        return attributes;
    }

    //    public void addAttributeAnnotation (String attribute, String annotation){
//        if(matrix.containsKey(attribute)){
//            if(matrix.get(attribute).containsKey(annotation)){
//                matrix.get(attribute).put(annotation, matrix.get(attribute).get(annotation) + 1);
//            }
//            else{
//                matrix.get(attribute).put(annotation, 1);
//            }
//        }
//        else{
//            matrix.put(attribute, new TreeMap<>());
//            matrix.get(attribute).put(annotation, 1);
//        }
//    }

//    public void merge(AnnotationMatrix annotationMatrix) {
//        annotationMatrix.getMatrix().forEach((s, map) -> {
//            if (!matrix.containsKey(s)) {
//                matrix.put(s, map);
//            } else {
//                map.forEach((s1, v) -> matrix.get(s1).merge(s1, v, (v1, v2) -> v1 + v2));
//            }
//        });
//    }

    @Override
    public String toString() {
        return Arrays.toString(matrix.entrySet().stream()
                .map(e -> (e.getKey() + "=" + Arrays.toString(e.getValue().entrySet().stream()
                        .map(e1 -> e1.getKey() + "=" + e1.getValue().size())
                        .toArray())))
                .toArray()) + '}';
    }

    public void addToAnnotationMatrix(VertexAttribute attribute) {
        matrix.put(attribute.getAccession(), attribute.getAnnotationMap().getMap());
        attributes.put(attribute.getAccession(), attribute.getInstances());
    }

    public void interference(){
        Set<Set<String>> attributeCombinations = new CombinationMaker<>(new ArrayList<>(attributes.keySet())).getCombinations();
        attributeCombinations.stream().filter(set -> set.size() > 1).filter(set -> set.size() < 4).forEach(set -> {
            String key = set.toString();
            SortedSet<String> instances = new TreeSet<>();
            Map<String, SortedSet<String>> map = new TreeMap<>();
            Map<String, Integer> annotPerSet = new TreeMap<>();
            set.forEach(attribute -> {
                if(instances.isEmpty()){
                    instances.addAll(attributes.get(attribute));
                }
                else{
                    instances.retainAll(attributes.get(attribute));
                }

                matrix.get(attribute).entrySet().forEach(e ->{
                    String annotation = e.getKey();
                    if(!annotPerSet.containsKey(annotation)){
                        annotPerSet.put(annotation, 1);
                    }
                    annotPerSet.put(annotation, annotPerSet.get(annotation) + 1);
                    SortedSet<String> annotationInstances = e.getValue();


                    if(!map.containsKey(annotation)){
                        map.put(annotation, new TreeSet<>());
                    }
                    if(map.get(annotation).isEmpty()){
                        map.get(annotation).addAll(annotationInstances);
                    }
                    else{
                        map.get(annotation).retainAll(annotationInstances);
                    }
                });
            });
            for(Iterator<Map.Entry<String, SortedSet<String>>> iterator = map.entrySet().iterator(); iterator.hasNext(); ){
                Map.Entry<String, SortedSet<String>> entry = iterator.next();
                if(annotPerSet.get(entry.getKey()) != set.size()){
                    iterator.remove();
                }
            }

            attributes.put(key, instances);
            matrix.put(key, map);

        });

     }

    public void interferences(){
        Set<Set<Map.Entry<String, SortedSet<String>>>> attributeCombinations = new CombinationMaker<>(new ArrayList<>(attributes.entrySet())).getCombinations();
        attributeCombinations.stream().filter(set -> set.size() > 1).forEach(set -> {
            String key = set.stream().map(Map.Entry::getKey).collect(Collectors.toSet()).toString();
            SortedSet<String> instances = new TreeSet<>();
            set.forEach(e ->  {
                if(instances.isEmpty()){
                    instances.addAll(e.getValue());
                }
                else{
                    instances.retainAll(e.getValue());
                }
            });
            attributes.put(key, instances);
        });

        Set<Set<Map.Entry<String, Map<String, SortedSet<String>>>>> matrixhCombinations = new CombinationMaker<>(new ArrayList<>(matrix.entrySet())).getCombinations();
        matrixhCombinations.stream().filter(set -> set.size() > 1).forEach(set -> {
            String key = set.stream().map(Map.Entry::getKey).collect(Collectors.toSet()).toString();
            Map<String, SortedSet<String>> map = new TreeMap<>();
            set.forEach(stringMapEntry -> {
                stringMapEntry.getValue().forEach((annotation, instances) ->{
                    if(!map.containsKey(annotation)){
                        map.put(annotation, new TreeSet<>());
                    }
                    if(map.get(annotation).isEmpty()){
                        map.get(annotation).addAll(instances);
                    }
                    else{
                        map.get(annotation).retainAll(instances);
                    }

                });
            });
            matrix.put(key, map);
        });
    }
}
