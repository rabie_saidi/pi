package pi.core.signal;

import scala.Serializable;

import java.util.*;

/**
 * @author Rabie Saidi
 */
public class AnnotationMap implements Serializable{
    private Map<String, SortedSet<String>> map = new TreeMap<>();

    public Map<String, SortedSet<String>> getMap() {
        return map;
    }

    public void addAnnotation (String annotation, String accession){
        if(!map.containsKey(annotation)){
            map.put(annotation, new TreeSet<>());
        }
        map.get(annotation).add(accession);
    }

    public void merge (AnnotationMap annotationMap){
        annotationMap.getMap().forEach((annotation, accessions) ->
            accessions.forEach(accession -> addAnnotation(annotation, accession)));

    }

    @Override
    public String toString() {
        return Arrays.toString(map.entrySet().stream()
                .map(e -> e.getKey() + "=" + e.getValue().size() + " " + e.getValue())
                .toArray()) + '}';
    }
}
