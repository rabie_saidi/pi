package pi.core.signal;

import la.matrix.DenseMatrix;
import la.matrix.Matrix;
import pi.core.prediction.PredictionSet;
import pi.optimisation.OptimisationMatrix;
import pi.optimisation.Optimiser;
import pi.optimisation.OptimiserALM;
import pi.optimisation.OptimiserCorrelation;
import scala.Serializable;

import java.util.*;

import static java.lang.Thread.sleep;
import static ml.utils.Printer.disp;

/**
 * @author Rabie Saidi
 */
public class PredictionMaker implements Serializable{
    public PredictionSet predict(String entry, AnnotationMatrix annotationMatrix){
        OptimisationMatrix optimisationMatrix = convert(entry, annotationMatrix);
        //Optimiser optimiser = new OptimiserBase(optimisationMatrix, 2000, TimeUnit.MILLISECONDS);
        Optimiser optimiser = new OptimiserCorrelation(optimisationMatrix);
        return optimiser.predict();
    }

    private OptimisationMatrix convert(String entry, AnnotationMatrix annotationMatrix) {
        List<String> annotations = extractAnnotations(annotationMatrix);
        List<String> attributes = extractAttributes(annotationMatrix);
        Matrix data = extractData(annotationMatrix, annotations, attributes);
//        try {
//            sleep(10);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println("\nOptimising signals for: " + entry + " (" + data.getRowDimension() + "," + data.getColumnDimension() + ")\n");
//        disp(data);
//        try {
//            sleep(10);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return new OptimisationMatrix(entry, data, annotations, attributes);
    }

    private List<String> extractAnnotations(AnnotationMatrix annotationMatrix) {
        SortedSet<String> annotationSet = new TreeSet<>();
        annotationMatrix.getMatrix().forEach((attribute, annotationMap) -> annotationSet.addAll(annotationMap.keySet()));
        return new ArrayList<>(annotationSet);
    }


    private List<String> extractAttributes(AnnotationMatrix annotationMatrix) {
        annotationMatrix.interference();
        SortedSet<String> attributeSet = new TreeSet<>();
        attributeSet.addAll(annotationMatrix.getMatrix().keySet());
        return new ArrayList<>(attributeSet);
    }

    private Matrix extractData(AnnotationMatrix annotationMatrix, List<String> annotations, List<String> attributes) {
        double[][] scores = new double[1+attributes.size()][1+attributes.size()+annotations.size()];
        for(int i=0 ; i<=attributes.size()+annotations.size() ; i++){
            scores[0][i] = i<=attributes.size() ? 1.0 : 0.0;
        }
        int i = 1;
        for(String attribute : attributes){
            Map<String, SortedSet<String>> map = annotationMatrix.getMatrix().get(attribute);
            int j = 0;
            scores[i][j] = 1.0;
            for(String attribute1 : attributes){
                j++;
                double score = 1.0;
                if(!attribute.equals(attribute1)){
                    Set<String>  intersection = new HashSet<>();
                    intersection.addAll(annotationMatrix.getAttributes().get(attribute));
                    intersection.retainAll(annotationMatrix.getAttributes().get(attribute1));
                    score = (double) intersection.size() / annotationMatrix.getAttributes().get(attribute).size();
                }
                scores[i][j] = score;
            }
            for(String annotation : annotations){
                double score = 0.0;
                if(map.containsKey(annotation)){
                    score = (double) map.get(annotation).size() / annotationMatrix.getAttributes().get(attribute).size();
                }
                scores[i][j] = score;
                j++;
            }
            i++;
        }
        return new DenseMatrix(scores);
    }

}
