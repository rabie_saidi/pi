package pi.core.aggregate;

import org.apache.spark.graphx.EdgeContext;
import pi.core.vertex.Vertex;
import pi.core.vertex.VertexProtein;
import pi.core.vertex.VertexProteinType;
import pi.core.vertex.VertexType;
import pi.core.edge.EdgeProperty;
import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/**
 * @author Rabie Saidi
 */
public class SendV extends AbstractFunction1<EdgeContext<Vertex, EdgeProperty, Vertex>, BoxedUnit>
        implements Serializable {
    @Override
    public BoxedUnit apply(EdgeContext<Vertex, EdgeProperty, Vertex> context) {
        if (context.srcAttr().getVertexType() == VertexType.NONE || context.dstAttr().getVertexType() == VertexType.NONE) {
            return null;
        }

        if (context.srcAttr().getAnnotationMap().getMap().isEmpty()) {
            return null;
        }

        if (context.dstAttr().getVertexType() == VertexType.INSTANCE){
            if(((VertexProtein)context.dstAttr()).getVertexProteinType() == VertexProteinType.UNKNOWN_INSTANCE) {
                context.sendToDst(context.dstAttr());
                return null;
            }
        }
        context.dstAttr().merge(context.srcAttr().getAnnotationMap());
        context.dstAttr().addToFrom(context.srcAttr().getAccession());
        context.sendToDst(context.dstAttr());
        return null;
    }
}
