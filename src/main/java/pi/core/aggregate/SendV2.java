package pi.core.aggregate;

import org.apache.hadoop.yarn.webapp.hamlet.HamletSpec;
import org.apache.spark.graphx.EdgeContext;
import pi.core.edge.EdgeProperty;
import pi.core.vertex.Vertex;
import pi.core.vertex.VertexAttribute;
import pi.core.vertex.VertexProtein;
import pi.core.vertex.VertexType;
import scala.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/**
 * @author Rabie Saidi
 */
public class SendV2 extends AbstractFunction1<EdgeContext<Vertex, EdgeProperty, Vertex>, BoxedUnit>
        implements Serializable {
    @Override
    public BoxedUnit apply(EdgeContext<Vertex, EdgeProperty, Vertex> context) {
        if (context.srcAttr().getVertexType() == VertexType.NONE || context.dstAttr().getVertexType() == VertexType.NONE) {
            return null;
        }

        if (context.srcAttr().getAnnotationMap().getMap().isEmpty()) {
            return null;
        }

        context.dstAttr().merge(context.srcAttr().getAnnotationMap());
        if(context.dstAttr().getVertexType() == VertexType.INSTANCE
                && context.srcAttr().getVertexType() == VertexType.ATTRIBUTE){
            VertexProtein protein = (VertexProtein) context.dstAttr();
            VertexAttribute attribute = (VertexAttribute) context.srcAttr();

            protein.addToAnnotationMatrix(attribute);
        }
        context.dstAttr().addToFrom(context.srcAttr().getAccession());
        context.sendToDst(context.dstAttr());
        return null;
    }
}
