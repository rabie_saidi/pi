package pi.core.aggregate;

import pi.core.vertex.Vertex;
import scala.Serializable;
import scala.runtime.AbstractFunction2;

/**
 * @author Rabie Saidi
 */
public class MergeV extends AbstractFunction2<Vertex, Vertex, Vertex> implements Serializable {
    @Override
    public Vertex apply(Vertex vertex, Vertex vertex2) {
        if(!vertex.toString().equals(vertex2.toString())){
            vertex.merge(vertex2.getAnnotationMap());
            vertex.addToFrom(vertex2.getFrom());

        }
        return vertex;
    }
}
