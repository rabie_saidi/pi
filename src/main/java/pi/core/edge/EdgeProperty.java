package pi.core.edge;

import scala.Serializable;

/**
 * @author Rabie Saidi
 */
public class EdgeProperty implements Serializable{
    private String vertex1;
    private String vertex2;

    public EdgeProperty(String vertex1, String vertex2) {
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
    }

    @Override
    public String toString() {
        return "EdgeProperty{" +
                "vertex1='" + vertex1 + '\'' +
                ", vertex2='" + vertex2 + '\'' +
                '}';
    }
}
