package pi.util;

import com.google.common.hash.Hashing;
import org.apache.commons.collections.comparators.ComparableComparator;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.graphx.*;
import org.apache.spark.rdd.RDD;
import org.apache.spark.storage.StorageLevel;
import pi.core.edge.EdgeProperty;
import pi.core.prediction.PredictionSet;
import pi.core.signal.AnnotationMap;
import pi.core.signal.PredictionMaker;
import pi.core.vertex.*;
import scala.Function1;
import scala.Function2;
import scala.Serializable;
import scala.Tuple2;
import scala.math.Ordering;
import scala.math.Ordering$;
import scala.reflect.ClassTag;
import scala.reflect.ClassTag$;
import scala.runtime.BoxedUnit;

import java.nio.charset.Charset;
import java.util.*;

/**
 * @author Rabie SAIDI
 */
public class Util implements Serializable{
    private ClassTag<Vertex> vertexClassTag = ClassTag$.MODULE$.apply(Vertex.class);
    private ClassTag<EdgeProperty> edgeClassTag = ClassTag$.MODULE$.apply(EdgeProperty.class);
    private ClassTag<AnnotationMap> AnnotationMapClassTag = ClassTag$.MODULE$.apply(AnnotationMap.class);
    private Ordering ord = Ordering$.MODULE$.comparatorToOrdering(new ComparableComparator());

    public Graph<Vertex, EdgeProperty> createGraph(JavaSparkContext sc, String uniprotInput, double rate) {
        JavaRDD<String> entryTexts = sc.textFile(uniprotInput);
        JavaRDD<String> entryTextsSample = rate == 1 ? entryTexts : entryTexts.sample(false, rate);
        JavaRDD<Vertex> unpVertices = entryTextsSample.map(v1 -> new VertexFactory().createVertexProtein(v1));
        //System.out.println("Sample size: " + entryTextsSample.count());

        JavaPairRDD<String, Vertex> sigProtPairs = unpVertices.flatMapToPair(proteinEntry -> {
            List<Tuple2<String, Vertex>> tuples = new ArrayList<>();
            ((VertexProtein)proteinEntry).getSignatures().forEach(sig -> tuples.add(new Tuple2<>(sig, proteinEntry)));
            return tuples;
        });
        JavaRDD<Vertex> iprVertices = sigProtPairs.groupByKey()
                .mapToPair(pair -> new Tuple2<Vertex, Iterable<Vertex>>(
                        new VertexFactory().createVertexAttribute(pair), pair._2))
                .keys();
//        JavaRDD<Vertex> iprVertices = sigProtPairs.groupByKey().keys().map(VertexAttribute::new);

        //Union the two types of vertices
        JavaRDD<Vertex> unionJavaRDD = unpVertices.union(iprVertices);

        JavaRDD<Tuple2<Object, Vertex>> vertexJavaRDD = unionJavaRDD.map(vertex -> {
            long id = generateHash(vertex.getAccession());
            return new Tuple2<>(id, vertex);
        });

        VertexRDD<Vertex> vertexRDD = VertexRDD.apply(vertexJavaRDD.rdd(), vertexClassTag);

        JavaRDD<Edge<EdgeProperty>> edgeJavaRDD = sigProtPairs.map(pair -> {
            VertexProtein protein = (VertexProtein) pair._2;
            String attribute = pair._1;
            long proteinId = generateHash(protein.getAccession());
            long attributeId = generateHash(attribute);
            if (protein.getVertexProteinType() == VertexProteinType.UNKNOWN_INSTANCE) {
                return new Edge<>(attributeId, proteinId, new EdgeProperty(attribute, protein.getAccession()));
            }
            return new Edge<>(proteinId, attributeId, new EdgeProperty(protein.getAccession(), attribute));

        });
        EdgeRDD<EdgeProperty> edgeRDD = EdgeRDD.fromEdges(edgeJavaRDD.rdd(), edgeClassTag, vertexClassTag);

        return createGraph(vertexRDD, edgeRDD);
    }

    public void saveTripletsAsTextFile(RDD<EdgeTriplet<Vertex, EdgeProperty>> triplets, String output){
        triplets.repartition(1, ord).saveAsTextFile(output);
    }

    public void saveVerticesAsTextFile(VertexRDD<Vertex> vertexRDD, String output){
        vertexRDD.repartition(1, ord).saveAsTextFile(output);
    }

    public VertexRDD<Vertex> aggregateMessages(Graph<Vertex, EdgeProperty> graph,
                                               Function1<EdgeContext<Vertex, EdgeProperty, Vertex>, BoxedUnit> send,
                                               Function2<Vertex, Vertex, Vertex> merge) {
        return graph.aggregateMessages(
                send,
                merge,
                new TripletFields(true, true, true),
                vertexClassTag
        );
    }

    public Graph<Vertex, EdgeProperty> createGraph(VertexRDD<Vertex> vertices, EdgeRDD<EdgeProperty> edges) {
        return Graph.apply(vertices, edges, new VertexDefault(),
                StorageLevel.MEMORY_ONLY(), StorageLevel.MEMORY_ONLY(),
                vertexClassTag, edgeClassTag);
    }


//    public JavaRDD<String> getPlotData(VertexRDD<Vertex> vertexRDD) {
//        return vertexRDD.toJavaRDD().map(pair -> {
//            SortedSet<String> colours = new TreeSet<>();
//            pair._2.getAnnotationMap().getMap().forEach((s, strings) -> {
//                if(strings.size() > 30){
//                    colours.add(s.split("\\.")[0]);
//                }
//            });
//            if(colours.size() > 0) {
//                StringBuilder colourIndices = new StringBuilder();
//                colours.forEach(s -> colourIndices.append(s).append(","));
//                colourIndices.toString();
//                colourIndices.deleteCharAt(colourIndices.toString().length() - 1);
//                return pair._2.getAccession() + "\t" + colourIndices.toString();
//            }
//            else
//                return pair._2.getAccession() + "\t0";
//        });
//    }

    private long generateHash(String accession) {
        return Hashing.md5().hashString(accession, Charset.defaultCharset()).asLong();
    }


    public JavaRDD<PredictionSet> getPredictions(VertexRDD<Vertex> vertexRDD) {
        return vertexRDD.toJavaRDD().map(pair ->
            new PredictionMaker().predict(pair._2.getAccession(), ((VertexProtein)pair._2).getAnnotationMatrix()))
                .filter(predictionSet -> !predictionSet.isEmpty());
    }

    public void savePredictionsAsTextFile(JavaRDD<PredictionSet> javaRDD, String s) {
        javaRDD.repartition(1).saveAsTextFile(s);
    }

    public JavaRDD<String> getPlotData(JavaRDD<PredictionSet> predictions) {
        return predictions.map(predictionSet -> {
            StringBuilder line = new StringBuilder();
            String entry = predictionSet.getPredictions().get(0).split("\\t")[3];
            line.append(entry).append("\t");
            Set<String> colours = new HashSet<>();
            predictionSet.getPredictions().forEach(prediction -> {
                String ec = prediction.split("\\t")[2];
                colours.add(ec.split("\\.")[0]);
            });
            colours.forEach(s -> line.append(s).append(","));
            line.toString();
            line.deleteCharAt(line.toString().length() - 1);

            return line.toString();
        });
    }
}
