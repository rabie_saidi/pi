package pi.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author apapkova
 */

public class CombinationMaker<T> {
    private List<T> originalList;

    public CombinationMaker(List<T> originalList) {
        this.originalList = originalList;
    }

    public Set<Set<T>> getCombinations() {
        Set<Set<T>> combinations = new HashSet<>();
        int size = this.originalList.size();
        int threshold = Double.valueOf(Math.pow(2, size)).intValue() - 1;

        for (int i = 1; i <= threshold; ++i) {
            Set<T> individualCombinationList = new TreeSet<>();
            int count = size - 1;
            int clonedI = i;
            while (count >= 0) {
                if ((clonedI & 1) != 0) {
                    individualCombinationList.add(this.originalList.get(count));
                }
                clonedI = clonedI >>> 1;
                --count;
            }
            combinations.add(individualCombinationList);

//            if(i==threshold) {
//                System.out.println(combinations);
//            }
        }
        return combinations;
    }
}