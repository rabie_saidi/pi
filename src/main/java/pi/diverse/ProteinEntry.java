package pi.diverse;

import scala.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rabie Saidi
 */
public class ProteinEntry implements Serializable{
    private String id;
    private String annotation;
    private List<String> signatures;
    private List<String> neigbours = new ArrayList<>();

    public ProteinEntry(String id, List<String> signatures) {
        this.id = id;
        this.signatures = signatures;
    }

    public ProteinEntry(String id, String annotation, List<String> signatures) {
        this.id = id;
        this.annotation = annotation;
        this.signatures = signatures;
    }

    public void addNeighbour(String accession){
        neigbours.add(accession);
    }

    public void addNeighbours(List<String> accessions){
        accessions.forEach(accession -> {if(!neigbours.contains(accession))neigbours.add(accession);});
    }

    public void setNeigbours(List<String> neigbours) {
        this.neigbours = neigbours;
    }

    public String getId() {
        return id;
    }

    public String getAnnotation() {
        return annotation;
    }

    public List<String> getSignatures() {
        return signatures;
    }

    public List<String> getNeigbours() {
        return neigbours;
    }

    @Override
    public String toString() {
        return "ProteinEntry    " + id + '\'' +
                ", annotation='" + annotation + '\'' +
                ", neigbours=" + neigbours +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProteinEntry)) return false;

        ProteinEntry that = (ProteinEntry) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
