package pi.diverse;

import scala.Serializable;
import uk.ac.ebi.kraken.interfaces.uniprot.DatabaseType;
import uk.ac.ebi.kraken.interfaces.uniprot.UniProtEntry;
import uk.ac.ebi.kraken.model.factories.DefaultUniProtFactory;
import uk.ac.ebi.kraken.parser.UniProtParser;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rabie Saidi
 */
public class ProteinEntryParser implements Serializable{
    public static final String EOL_UNIX = "\n";
    public static final String FF_ENTRY_DELIMITER = "//";
    public ProteinEntry parse(String s) throws Exception {
        String entryString = new StringBuilder(s.trim())
                .append(EOL_UNIX)
                .append(FF_ENTRY_DELIMITER)
                .toString();
        UniProtEntry uniProtEntry = UniProtParser.parse(entryString, DefaultUniProtFactory.getInstance());
        String entryId = uniProtEntry.getPrimaryUniProtAccession().getValue();

        String annotation;
        try {
            annotation = uniProtEntry.getProteinDescription().getEcNumbers().get(0);
        } catch (Exception e) {
            annotation = "NONE";
        }
        List<String> iprs = new ArrayList<>();
        uniProtEntry.getDatabaseCrossReferences(DatabaseType.INTERPRO).forEach(
                ipr -> iprs.add(ipr.getPrimaryId().getValue())
        );

        ProteinEntry proteinEntry = new ProteinEntry(entryId, annotation, iprs);
        return proteinEntry;
    }
}
