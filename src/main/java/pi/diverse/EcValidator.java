package pi.diverse;

/**
 * Validator for EC numbers
 * @author Rabie Saidi
 */
public class EcValidator {
    private static String REGEX = "[1-6](\\.(\\-|\\d{1,2})){3}n*";
    public boolean validate(String ec){
        if(!ec.matches(REGEX)) return false;
        String[] digits = ec.split("\\.");
        boolean invalid = digits[1].equals("-") && !digits[2].equals("-") && !digits[3].equals("-")
                || digits[2].equals("-") && !digits[3].equals("-");
        return ! invalid;
    }

    public static void main(String[] args) {
        String ec = "1.2.3.5";
        System.out.println(new EcValidator().validate(ec));
    }
}
