package pi.diverse;

import com.google.common.collect.Sets;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.slf4j.LoggerFactory;
import pi.app.Config;
import scala.Serializable;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author Rabie Saidi
 */
public class SabeurData implements Serializable{
    static private org.slf4j.Logger log = LoggerFactory.getLogger(SabeurData.class.getName());

    public static void main(String[] args) {
        JavaSparkContext sc = Config.getSparkContext("local");
        String unpDelimiter = "//\n";
        sc.hadoopConfiguration().set("textinputformat.record.delimiter", unpDelimiter);

        String uniprotInput = args[0];
        String output = args[1];
        double rate = Double.parseDouble(args[2]);

        JavaRDD<String> entryTexts = sc.textFile(uniprotInput);
        JavaRDD entryTextsSample = rate == 1 ? entryTexts : entryTexts.sample(false, rate);

        JavaRDD unitProtEntryJavaRDD = entryTextsSample.map(new Function<String, ProteinEntry>() {
            @Override
            public ProteinEntry call(String s) throws Exception {
                return new ProteinEntryParser().parse(s);
            }
        });

        JavaPairRDD<ProteinEntry, ProteinEntry> allPairs = unitProtEntryJavaRDD.cartesian(unitProtEntryJavaRDD);
        JavaPairRDD<ProteinEntry, ProteinEntry> linkedPairs = allPairs.filter(new Function<Tuple2<ProteinEntry, ProteinEntry>, Boolean>() {
            @Override
            public Boolean call(Tuple2<ProteinEntry, ProteinEntry> pair) throws Exception {
                boolean intersection = !Sets.intersection(
                        new HashSet<>(pair._1.getSignatures()),
                        new HashSet<>(pair._2.getSignatures())).isEmpty();
                boolean notSame = !pair._1.getId().equals(pair._2.getId());
                return intersection && notSame;
            }
        });

        JavaPairRDD<ProteinEntry, Iterable<ProteinEntry>> proteinEntryIterableJavaPairRDD = linkedPairs.groupByKey();

        JavaRDD<ProteinEntry> proteinEntryJavaRDD = proteinEntryIterableJavaPairRDD.map(new Function<Tuple2<ProteinEntry, Iterable<ProteinEntry>>, ProteinEntry>() {
            @Override
            public ProteinEntry call(Tuple2<ProteinEntry, Iterable<ProteinEntry>> pair) throws Exception {
                List<String> neighbours = new ArrayList<>();
                pair._2.forEach(proteinEntry -> neighbours.add(proteinEntry.getId()));
                pair._1.setNeigbours(neighbours);
                return pair._1;
            }
        });

        proteinEntryJavaRDD.repartition(1).saveAsTextFile(output);



    }
}
