package pi.diverse;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.slf4j.LoggerFactory;
import pi.app.Config;
import scala.Serializable;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rabie Saidi
 */
public class SabeurData2 implements Serializable{
    static private org.slf4j.Logger log = LoggerFactory.getLogger(SabeurData2.class.getName());

    public static void main(String[] args) {
        JavaSparkContext sc = Config.getSparkContext("local");
        String unpDelimiter = "//\n";
        sc.hadoopConfiguration().set("textinputformat.record.delimiter", unpDelimiter);

        String uniprotInput = args[0];
        String output = args[1];
        double rate = Double.parseDouble(args[2]);

        JavaRDD<String> entryTexts = sc.textFile(uniprotInput);
        JavaRDD<String> entryTextsSample = rate == 1 ? entryTexts : entryTexts.sample(false, rate);

        JavaRDD<ProteinEntry> unitProtEntryJavaRDD = entryTextsSample.map(new Function<String, ProteinEntry>() {
            @Override
            public ProteinEntry call(String s) throws Exception {
                return new ProteinEntryParser().parse(s);
            }
        });
        //System.out.println("Number of proteins:" + unitProtEntryJavaRDD.count());


        JavaPairRDD<String, ProteinEntry> sigProtPairs = unitProtEntryJavaRDD.flatMapToPair(new PairFlatMapFunction<ProteinEntry, String, ProteinEntry>() {
            @Override
            public Iterable<Tuple2<String, ProteinEntry>> call(ProteinEntry proteinEntry) throws Exception {
                List<Tuple2<String, ProteinEntry>> tuples = new ArrayList<Tuple2<String, ProteinEntry>>();
                proteinEntry.getSignatures().forEach(sig -> tuples.add(new Tuple2<>(sig, proteinEntry)));
                return tuples;
            }
        });

        JavaPairRDD<String, Iterable<ProteinEntry>> sigProteinsPairs = sigProtPairs.groupByKey().mapToPair(new PairFunction<Tuple2<String, Iterable<ProteinEntry>>, String, Iterable<ProteinEntry>>() {
            @Override
            public Tuple2<String, Iterable<ProteinEntry>> call(Tuple2<String, Iterable<ProteinEntry>> sigProteinsPair) throws Exception {
                sigProteinsPair._2.forEach(proteinEntry
                        -> sigProteinsPair._2.forEach(proteinEntry1
                        -> {if(!proteinEntry.getId().equals(proteinEntry1.getId()))
                            proteinEntry.addNeighbour(proteinEntry1.getId());}));
                return sigProteinsPair;
            }
        });

        //sigProteinsPairs.repartition(1).saveAsTextFile(output);
        JavaPairRDD<String, ProteinEntry> proteins = sigProteinsPairs.values().flatMapToPair(new PairFlatMapFunction<Iterable<ProteinEntry>, String, ProteinEntry>() {
            @Override
            public Iterable<Tuple2<String, ProteinEntry>> call(Iterable<ProteinEntry> proteinEntries) throws Exception {
                List<Tuple2<String, ProteinEntry>> list = new ArrayList<Tuple2<String, ProteinEntry>>();
                proteinEntries.forEach(proteinEntry -> list.add(new Tuple2<>(proteinEntry.getId(), proteinEntry)));
                return list;
            }
        }).reduceByKey(new Function2<ProteinEntry, ProteinEntry, ProteinEntry>() {
            @Override
            public ProteinEntry call(ProteinEntry proteinEntry, ProteinEntry proteinEntry2) throws Exception {
                proteinEntry.addNeighbours(proteinEntry2.getNeigbours());
                return proteinEntry;
            }
        });
        proteins.values().repartition(1).saveAsTextFile(output);
//        JavaRDD<ProteinEntry> proteinEntryJavaRDD = sigProteinsPairs.values().flatMap(new FlatMapFunction<Iterable<ProteinEntry>, ProteinEntry>() {
//            @Override
//            public Iterator<ProteinEntry> call(Iterable<ProteinEntry> proteinEntries) throws Exception {
//                return proteinEntries.iterator();
//            }
//        });
//        proteinEntryJavaRDD.repartition(1).saveAsTextFile(output);
//
//        JavaPairRDD<ProteinEntry, ProteinEntry> allPairs = unitProtEntryJavaRDD;//.cartesian(unitProtEntryJavaRDD);
//        JavaPairRDD<ProteinEntry, ProteinEntry> linkedPairs = allPairs.filter(new Function<Tuple2<ProteinEntry, ProteinEntry>, Boolean>() {
//            @Override
//            public Boolean call(Tuple2<ProteinEntry, ProteinEntry> pair) throws Exception {
//                boolean intersection = !Sets.intersection(
//                        new HashSet<>(pair._1.getSignatures()),
//                        new HashSet<>(pair._2.getSignatures())).isEmpty();
//                boolean notSame = !pair._1.getAccession().equals(pair._2.getAccession());
//                return intersection && notSame;
//            }
//        });
//
//        JavaPairRDD<ProteinEntry, Iterable<ProteinEntry>> proteinEntryIterableJavaPairRDD = linkedPairs.groupByKey();
//
//        JavaRDD<ProteinEntry> proteinEntryJavaRDD = proteinEntryIterableJavaPairRDD.map(new Function<Tuple2<ProteinEntry, Iterable<ProteinEntry>>, ProteinEntry>() {
//            @Override
//            public ProteinEntry call(Tuple2<ProteinEntry, Iterable<ProteinEntry>> pair) throws Exception {
//                List<String> neighbours = new ArrayList<>();
//                pair._2.forEach(proteinEntry -> neighbours.add(proteinEntry.getAccession()));
//                pair._1.setNeigbours(neighbours);
//                return pair._1;
//            }
//        });
//
//        proteinEntryJavaRDD.repartition(1).saveAsTextFile(output);



    }
}
